# MC-197883 Fix

[![pipeline status](https://gitlab.com/stellardrift/mc197883/badges/dev/pipeline.svg)](https://gitlab.com/stellardrift/mc197883/-/commits/dev)

A quick port of Paper's fix for the world data conversion issue from https://github.com/PaperMC/Paper/blob/1ab021ddca9dc2bd48ce4308cccb7440528af25a/Spigot-Server-Patches/0556-MC-197883-Bandaid-decode-issue.patch

Works in dev + in production, no configuration... drop it in (BEFORE loading worlds) and go.

There are still some migration issues, but this takes care of the biggest one.

The original patch was released under the terms of the MIT license, and so is this mod.


