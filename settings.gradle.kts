pluginManagement {
    repositories {
        maven(url = "https://maven.fabricmc.net/") {
            name = "fabric"
        }
        gradlePluginPortal()
    }
}

rootProject.name = "mc197883"
