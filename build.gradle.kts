import ca.stellardrift.build.configurate.ConfigFormats
import ca.stellardrift.build.configurate.transformations.convertFormat

plugins {
    val opinionatedVersion = "4.1"
    val indraVersion = "1.2.1"
    id("ca.stellardrift.opinionated.fabric") version opinionatedVersion
    id("net.kyori.indra.publishing.bintray") version indraVersion
    id("ca.stellardrift.configurate-transformations") version opinionatedVersion
    id("com.github.ben-manes.versions") version "0.36.0"
}

group = "ca.stellardrift"
version = "1.1-SNAPSHOT"

repositories {
    jcenter()
}

indra {
    gitlab("stellardrift", "mc197883") {
        ci = true
    }

    mitLicense()
}

val versionMinecraft: String by project
val versionYarn: String by project
val versionLoader: String by project

dependencies {
    minecraft("com.mojang:minecraft:$versionMinecraft")
    mappings("net.fabricmc:yarn:$versionMinecraft+build.$versionYarn:v2")
    modImplementation("net.fabricmc:fabric-loader:$versionLoader")
}

tasks.withType(ProcessResources::class).configureEach {
    filesMatching("fabric.mod.yml") {
        expand("project" to project)
        convertFormat(ConfigFormats.YAML, ConfigFormats.JSON)
        name = "fabric.mod.json"
    }

    filesMatching("*.mixins.json") {
        expand("project" to project)
    }
}
