/*
 * Copyright (c) 2020 zml (mod) and Aikar (original fix)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ca.stellardrift.mc197883.mixin;

import com.mojang.serialization.MapLike;
import com.mojang.serialization.codecs.KeyDispatchCodec;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

/**
 * Mojang is inconsistent about usage of {@code name} and {@code type} as identifiers for type of a polymorphic container
 *
 * @param <K> key type
 * @param <V> value type
 */
@Mixin(value = KeyDispatchCodec.class, remap = false)
public class KeyDispatchCodecMixin<K, V> {

    @Redirect(method = "decode", at = @At(value = "INVOKE", target = "Lcom/mojang/serialization/MapLike;get(Ljava/lang/String;)Ljava/lang/Object;"))
    private <T> T correctInconsistentNameTypeUsage(final MapLike<T> input, final String typeKey) {
        final T elementName = input.get(typeKey);
        if (elementName == null && "type".equals(typeKey)) {
            return input.get("name");
        }
        return elementName;
    }
}
